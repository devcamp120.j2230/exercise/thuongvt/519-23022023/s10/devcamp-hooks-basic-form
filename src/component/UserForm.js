import { useEffect, useState } from "react"


function UserForm() {

    const[firstName, setFirstName] = useState(localStorage.getItem("firstName")||"");
    const[lastName, setLastName] = useState(localStorage.getItem("lastName")||"")


    const onChangdlerFirstNameChange = (event)=>{
        console.log(event.target.value);
        setFirstName(event.target.value);
    }

    const onChangdlerLastNameChange = (event)=>{
        console.log(event.target.value);
        setLastName(event.target.value);
    }

    useEffect(()=>{
        localStorage.setItem("firstName", firstName);
        localStorage.setItem("lastName",lastName);
    })

    return (
        <div className='row justify-content ' style={{ background: "blue" }}>
            <div>
                <input className="form-control mt-3" placeholder="devcamp" value={firstName} onChange={onChangdlerFirstNameChange}>
                </input>
            </div>
            <div>
                <input className="form-control mt-3" placeholder="user" value={lastName} onChange={onChangdlerLastNameChange} >
                </input>
            </div>
            <div>
                <p>{firstName} {lastName}</p>
            </div>
        </div>
    )
}

export default UserForm